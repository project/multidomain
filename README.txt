Module : Multi Domain
Author : Adrian Rossouw
Email  : adrian@bryght.com

This is a module which allows you to configure which domains to use for different
parts of your site. It is also useful for switching to different protocols (ie: http / https)
for different parts of your site (ie: put all the ecommerce or user account stuff behind https)

This module was primarily developed for www.oasismag.com, because I want to host
different types of content, on different sites, while using the same database, so that I can 
post information between the different sites, without having to worry about syndication
and the like.

This module requires the single sign on module, to allow you to be logged into multiple domains
at the same time.

This module will be extended in the future by userdomains.module, which will allow each user
to have their own sub domain on the site.
